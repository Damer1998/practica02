
import React, { Component } from 'react';

import { StyleSheet, View, Text, 
  TouchableOpacity,
  Image,  Button } from 'react-native';

  const imageUrl = 'https://www.soin-et-nature.com/24042-menu_default/racecadotrilo-100-mg-biogaran-consejo-diarrea-10-capsulas.jpg';

export default class Primera extends Component {
  constructor(props) {
    super(props);
    this.state = {

      
    };
    

  }
  static navigationOptions = {
    title: 'Pastilla rasecadotril',
    
  };


  render() {
    const { navigate } = this.props.navigation;
    return (

      <View style={styles.listItem}  >

      <View style={styles.container}>
      <Image source={{uri:imageUrl}}  style={{width:300, height:200,borderRadius:20}} />

            <View style={{alignItems:"center",flex:1}}>
            
            <Text></Text>
              <Text style={styles.titulo}>Descripcion</Text>
      
              <Text style={styles.name} >Racecadotril es un agente antidiarreico que actúa
               como un inhibidor periférico de encefalinasa.​   </Text>
                 <Text style={styles.name} > A diferencia de otros medicamentos usados
                  para tratar la diarrea, que disminuyen la motilidad intestinal, 
racecadotril​ tiene un efecto antisecretorio, reduciendo la secreción de agua y 
electrólitos al intestino. </Text>      
            </View>
     
            <Button  title="Retroceder" onPress={() => navigate('HomeScreen')} />
      </View>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },

 

  titulo: {
    flex: 0.1,
    color: '#fafafa',
    fontWeight:"bold"
  },

  name: {
    flex: 0.3,
    color: '#fafafa'
  
  },



  listItem:{
    margin:12,
    padding:10,
    backgroundColor:"#00838f",
    width:"90%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  }
});
