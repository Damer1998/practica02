
import React, { Component } from 'react';

import { StyleSheet, View, Text, 
  TouchableOpacity,
  Image,  Button } from 'react-native';

  const imageUrl = 'https://ve.tumedico.com/assets/img/medicinas/vad.jpg';

export default class Segunda extends Component {
  constructor(props) {
    super(props);
    this.state = {

      
    };
    

  }

  // Estatico hasta nuevo inicio
  static navigationOptions = {
    title: 'Antisecretorio Butropina',
    
  };


  render() {
    const { navigate } = this.props.navigation;
    return (

      <View style={styles.listItem}  >

      <View style={styles.container}>
      <Image source={{uri:imageUrl}}  style={{width:300, height:200,borderRadius:20}} />

            <View style={{alignItems:"center",flex:1}}>
            
            <Text></Text>
              <Text style={styles.titulo}>Descripcion</Text>
      
              <Text style={styles.name} >Antisecretorio (coadyuvante) en el
               tratamiento de la úlcera péptica. Adyuvante en el síndrome del 
               colon irritable y otros desórdenes gastrointestinales funcionales.  
               </Text>
                 <Text style={styles.name} > (Dosis recomendada): 
                 Niños mayores de 2 años hasta 12 años:
                  5 mg (1 ml) cada 8 horas. Dosis máxima: 15 mg/día. </Text>      
            </View>
     
            <Button  title="Retroceder" onPress={() => navigate('HomeScreen')} />
      </View>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },

 

  titulo: {
    flex: 0.1,
    color: '#fafafa',
    fontWeight:"bold"
  },

  name: {
    flex: 0.3,
    color: '#fafafa'
  
  },



  listItem:{
    margin:12,
    padding:10,
    backgroundColor:"#b71c1c",
 
    width:"90%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  }
});
