
import React, { Component } from 'react';

import { StyleSheet, View, Text, 
  TouchableOpacity,
  Image,  Button } from 'react-native';

  const imageUrl = 'https://assets.metrolatam.com/cl/2016/05/03/screen-shot-2016-05-03-at-1-56-24-pm-1-1200x800.jpg';

export default class Primera extends Component {
  constructor(props) {
    super(props);
    this.state = {

      
    };
    

  }
  static navigationOptions = {
    title: 'Morfina',
    
  };


  render() {
    const { navigate } = this.props.navigation;
    return (

      <View style={styles.listItem}  >

      <View style={styles.container}>
      <Image source={{uri:imageUrl}}  style={{width:300, height:200,borderRadius:20}} />

            <View style={{alignItems:"center",flex:1}}>
            
            <Text></Text>
              <Text style={styles.titulo}>Descripcion</Text>
      
              <Text style={styles.name} >La morfina es una potente droga opiácea usada 
              frecuentemente en medicina como analgésico.  </Text>
                 <Text style={styles.name} > La morfina fue nombrada así por el farmacéutico alemán Friedrich Wilhelm Adam Sertürner
                  en honor a Morfeo  </Text>      
            </View>
     
            <Button  title="Retroceder" onPress={() => navigate('HomeScreen')} />
      </View>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },

 

  titulo: {
    flex: 0.1,
    color: '#fafafa',
    fontWeight:"bold"
  },

  name: {
    flex: 0.3,
    color: '#fafafa'
  
  },



  listItem:{
    margin:12,
    padding:10,
    backgroundColor:"#ff8f00",
    width:"90%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  }
});
