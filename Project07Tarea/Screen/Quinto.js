
import React, { Component } from 'react';

import { StyleSheet, View, Text, 
  TouchableOpacity,
  Image,  Button } from 'react-native';

  const imageUrl = 'https://www.ecured.cu/images/5/5a/Bupivaca%C3%ADna_clorhidrato.jpg';

export default class Primera extends Component {
  constructor(props) {
    super(props);
    this.state = {

      
    };
    

  }
  static navigationOptions = {
    title: 'Pastilla loperamida',
    
  };


  render() {
    const { navigate } = this.props.navigation;
    return (

      <View style={styles.listItem}  >

      <View style={styles.container}>
      <Image source={{uri:imageUrl}}  style={{width:300, height:200,borderRadius:20}} />

            <View style={{alignItems:"center",flex:1}}>
            
            <Text></Text>
              <Text style={styles.titulo}>Descripcion</Text>
      
              <Text style={styles.name} >La loperamida es un opioide y un derivado sintético de 
              la piperidina.​   </Text>
                 <Text style={styles.name} > Se trata de un fármaco efectivo contra la diarrea generada por una gastroenteritis o una 
enfermedad inflamatoria intestinal. </Text>      
            </View>
     
            <Button  title="Retroceder" onPress={() => navigate('HomeScreen')} />
      </View>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },

 

  titulo: {
    flex: 0.1,
    color: '#fafafa',
    fontWeight:"bold"
  },

  name: {
    flex: 0.3,
    color: '#fafafa'
  
  },



  listItem:{
    margin:12,
    padding:10,
    backgroundColor:"#ad1457",
    width:"90%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  }
});
