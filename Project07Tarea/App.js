
//Importaciones de  React
import React from 'react';

//Importar navegadores desde React Navigation
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

//Importa todas las pantallas necesarias
import SplashScreen from './Screen/SplashScreen';
import LoginScreen from './Screen/LoginScreen';
import RegisterScreen from './Screen/RegisterScreen';
import DrawerNavigationRoutes from './Screen/DrawerNavigationRoutes';

// Importacion de Screen Listado de Medicamentos

import PrimeraActividad from './Screen/Primera';
import SegundaActividad from './Screen/Segunda';
import TerceraActividad from './Screen/Tercera';
import CuartaActividad from './Screen/Cuarto';
import QuintaActividad from './Screen/Quinto';
import SextoActividad from './Screen/Sexto';
import SeptimoActividad from './Screen/Septimo';
import OctavoActividad from './Screen/Octavo';
import HomeScreen from './Screen/drawerScreens/HomeScreen';

import SettingsScreen from './Screen/drawerScreens/SettingsScreen';

// SettingsScreen


const Auth = createStackNavigator({
  //Stack Navigator para la pantalla de inicio de sesión y registro

  LoginScreen: {
    screen: LoginScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  RegisterScreen: {
    screen: RegisterScreen,
    navigationOptions: {
      title: 'Registro',
      headerStyle: {
        backgroundColor: '#307ecc',
      },
      headerTintColor: '#fff',
    },
  },
  
  HomeScreen: { screen: HomeScreen },
  SettingsScreen: { screen: SettingsScreen },  
  PrimeraActividad: { screen: PrimeraActividad }, 
  SegundaActividad: { screen: SegundaActividad }, 
  TerceraActividad: { screen: TerceraActividad },
  CuartaActividad: { screen: CuartaActividad }, 
  QuintaActividad: { screen: QuintaActividad }, 
  SextoActividad: { screen: SextoActividad }, 
  SeptimoActividad: { screen: SeptimoActividad }, 
  OctavoActividad: { screen: OctavoActividad }, 
}

// SettingsScreen


);

/* Cambie el Navegador para aquellas pantallas que deben cambiarse una sola vez
  y no queremos volver una vez que cambiemos de ellos a la siguiente   */
const App = createSwitchNavigator({ 
  SplashScreen: {
    /* SplashScreen que vendrá una vez durante 2 segundos*/
    screen: SplashScreen,
    navigationOptions: {
      /* Ocultar el encabezado de Splash Screen */
      headerShown: false,
    },
  },
  Auth: {
    /*  Auth Navigator que incluye el registro de inicio de sesión vendrá una vez */
    screen: Auth,
  },
  DrawerNavigationRoutes: {
    /* Cajón de navegación como página de destino */
    screen: DrawerNavigationRoutes,
    navigationOptions: {
      /* Ocultar el encabezado para el cajón de navegación, ya que vamos a utilizar nuestro encabezado personalizado */
      headerShown: false,
    },
  },
});

export default createAppContainer(App);